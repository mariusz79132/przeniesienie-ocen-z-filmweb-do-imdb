package chapula.main;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

import org.apache.log4j.Logger;

import chapula.filmweb.FwAPI;
import chapula.imdb.Imdb;
import info.talacha.filmweb.connection.FilmwebException;
import info.talacha.filmweb.models.Item;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.util.Pair;

public class App {

	final static Logger logger = Logger.getLogger(App.class);
	static {
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

	}

	public static void main(String[] args) throws Exception {
		new JFXPanel();
	
		Platform.setImplicitExit(false);

		logger.info("Program start");
		if (args.length == 0)
			System.out.println(
					"java -jar filmweb.jar [[-saveMovies filmwebUsername filmwebPassword] [-loadMovies imdbUsername imdbPassword] [-loadAndSave filmwebUsername filmwebPassword imdbUsername imdbPassword]]");

		else if (args[0].equals("-saveMovies")) {
			String filmwebUsername = args[1];
			String filmwebPassword = args[2];
			filmweb(filmwebUsername, filmwebPassword);

		} else if (args[0].equals("-loadMovies")) {
			String imbdUsername = args[1];
			String imbdPassword = args[2];
			imdb(imbdUsername, imbdPassword);
		} else if (args[0].equals("-loadAndSave")) {
			String filmwebUsername = args[1];
			String filmwebPassword = args[2];
			filmweb(filmwebUsername, filmwebPassword);

			String imbdUsername = args[3];
			String imbdPassword = args[4];
			imdb(imbdUsername, imbdPassword);
		} else {
			System.out.println(
					"java -jar filmweb.jar [[-saveMovies filmwebUsername filmwebPassword] [-loadMovies imdbUsername imdbPassword] [-loadAndSave filmwebUsername filmwebPassword imdbUsername imdbPassword]]");
		}
		
		Platform.exit();

	}

	private static void imdb(String imbdUsername, String imbdPassword) throws IOException {
		try (Imdb imbd = new Imdb(imbdUsername, imbdPassword)) {
			imbd.loadRates();

			try {
				imbd.signIn();
				imbd.rate();
			} catch (Exception e) {
				logger.error("Error: ", e);
			}
		}

	}

	private static void filmweb(String filmwebUsername, String filmwebPassword) {
		FwAPI fwApi = new FwAPI(filmwebUsername, filmwebPassword);
		List<Pair<Item, Integer>> fimls;
		try {
			fimls = fwApi.download();

			FwAPI.save(fimls);
		} catch (FilmwebException e) {
			logger.error(e);
		}
	}

}
