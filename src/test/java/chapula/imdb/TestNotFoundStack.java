package chapula.imdb;

import java.util.HashSet;
import java.util.Set;

import info.talacha.filmweb.models.ItemType;
import junit.framework.TestCase;

public class TestNotFoundStack extends TestCase {

	public void testNotFoundStack() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException,
			SecurityException, InterruptedException {

		IMDbMock mock = new IMDbMock();
		String response = "0\n";
		int size = 20;
		for (int i = 0; i < size; i++) {
			switch (i % 3) {
			case 0: {
				response += "0\n";
				break;
			}
			case 1: {
				response += "1\n";
				break;
			}
			case 2: {
				response += "p\n";
				break;
			}
			}
		}

		NotFoundStack notFoundStack = mock.getStack(response);

		for (int i = 0; i < size; i++) {

			Set<IMDbFilm> movies = new HashSet<>();
			movies.add(new IMDbFilm("13 powodow by " + i, "/test", 2017));
			movies.add(new IMDbFilm("13 powodow by 2 " + i, "/test", 2017));
			notFoundStack.addMovie(mock.makeItem("13 powodow by", 2017, ItemType.FILM), movies, 5);
		}
		notFoundStack.rate();

	}
}
