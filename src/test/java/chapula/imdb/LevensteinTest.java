package chapula.imdb;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

import chapula.string.Levenstein;
import info.talacha.filmweb.models.ItemType;
import javafx.util.Pair;
import junit.framework.TestCase;

public class LevensteinTest extends TestCase {
	IMDbMock mock = new IMDbMock();

	public void testLevenstein() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		System.out.println("testLevenstein");
		Set<IMDbFilm> testList = new HashSet<>();

		testList.add(new IMDbFilm("test2", "url1", 123));
		testList.add(new IMDbFilm("test2 asdasda", "url2", 12));
		testList.add(new IMDbFilm("te3st2", "url3", 123));
		testList.add(new IMDbFilm("test4212", "url4", 123));

		Imdb imdb = new Imdb("testU", "TestP");
		Class[] cArg = new Class[2];
		cArg[0] = String.class;
		cArg[1] = Set.class;

		List<Pair<IMDbFilm, Integer>> list = Levenstein.makeLevensteinList("test", testList);
		Levenstein.sort(list);
		list.forEach(item -> System.out.println(item.getKey().getName() + " lev: " + item.getValue()));

		assertEquals(list.get(0).getKey().getName(), "test2");
		assertEquals(list.get(1).getKey().getName(), "te3st2");
		assertEquals(list.get(2).getKey().getName(), "test4212");
		assertEquals(list.get(3).getKey().getName(), "test2 asdasda");

	}

	public void testTwoTitleLevenstein() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		System.out.println("testTwoTitleLevenstein");
		Set<IMDbFilm> testList = new HashSet<>();

		testList.add(new IMDbFilm("test2", "url1", 123));
		testList.add(new IMDbFilm("test2 asdasda", "url2", 12));
		testList.add(new IMDbFilm("te3st2", "url3", 123));
		testList.add(new IMDbFilm("test4212", "url4", 123));

		Class[] cArg = new Class[2];
		cArg[0] = String.class;
		cArg[1] = Set.class;

		List<Pair<IMDbFilm, Integer>> list = Levenstein.makeLevensteinList("test", testList);
		System.out.println("Normal");
		list.addAll(Levenstein.makeLevensteinList("te3st2", testList));
		list.forEach(item -> System.out.println(item.getKey().getName() + " lev: " + item.getValue()));
		Levenstein.sort(list);
		System.out.println("Sorted");
		list.forEach(item -> System.out.println(item.getKey().getName() + " lev: " + item.getValue()));
		Levenstein.removeDuplicates(list);
		System.out.println("Dup removed");
		list.forEach(item -> System.out.println(item.getKey().getName() + " lev: " + item.getValue()));

		assertEquals(list.get(0).getKey().getName(), "te3st2");
		assertEquals(list.get(1).getKey().getName(), "test2");
		assertEquals(list.get(2).getKey().getName(), "test4212");
		assertEquals(list.get(3).getKey().getName(), "test2 asdasda");

	}

	public void manualTestIntensive()
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, FailingHttpStatusCodeException, MalformedURLException, IOException {

		String title = "The Boy in the Striped Pyjamas";
		System.out.println(title + " URL: " + "http://www.imdb.com/find?ref_=nv_sr_fn&q="
				+ URLEncoder.encode(title, "UTF-8") + "&s=tt");
		assertEquals("/title/tt0914798/?ref_=fn_tt_tt_1", mock.returnUrl(title, 2008, 5, ItemType.FILM));
	}

	static {
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
	}

}
