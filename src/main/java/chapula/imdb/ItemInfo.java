package chapula.imdb;

import java.util.Set;

import info.talacha.filmweb.models.Item;

public class ItemInfo {
	private Item item;
	private Set<IMDbFilm> movies;
	private int rate;

	public ItemInfo(Item item, Set<IMDbFilm> movies, int rate) {
		super();
		this.item = item;
		this.movies = movies;
		this.rate = rate;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Set<IMDbFilm> getMovies() {
		return movies;
	}

	public void setMovies(Set<IMDbFilm> movies) {
		this.movies = movies;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}
}
