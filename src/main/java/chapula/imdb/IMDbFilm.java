package chapula.imdb;

import chapula.string.Preprocess;

public class IMDbFilm {
	private String name;
	private String url;
	private int year;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getName() {
		return name;
	}

	public String getPreprocessedName() {
		return Preprocess.preprocessTitle(name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public IMDbFilm(String name, String url, int year) {
		super();
		this.name = name;
		this.url = url;
		this.year = year;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		for (int i = 0; i < getUrl().length(); i++) {
			hash = hash * 31 + getUrl().charAt(i);
		}

		return hash;
	}

	@Override
	public boolean equals(Object o) {
		return ((IMDbFilm) o).getUrl().equals(this.getUrl());
	}
}
