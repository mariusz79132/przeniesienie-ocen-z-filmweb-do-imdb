package chapula.imdb;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.logging.Level;

import org.junit.Before;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

import chapula.string.Preprocess;
import info.talacha.filmweb.models.ItemType;
import junit.framework.TestCase;

public class ImdbTest extends TestCase {

	@Before
	public void initiate() {

	}

	static {
		java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
	}

	public void testPreprocessing() {
		assertEquals("asd", Preprocess.preprocessTitle("[[@][\"#!&'`%a*+()/-;s~:}?d>=<]].,\\"));
		assertEquals("asd", Preprocess.preprocessTitle("ASd"));
		assertEquals("lagiewza a a c c e e l l n n o o s s z z z z",
				Preprocess.preprocessTitle("łągiewża Ą ą Ć ć Ę ę Ł ł Ń ń Ó ó Ś ś Ź ź Ż ż"));
	}

	//
	public void testCharacters()
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, FailingHttpStatusCodeException, MalformedURLException, IOException {
		IMDbMock mock = new IMDbMock();
		assertEquals("/title/tt1038988/", mock.returnUrl("[rec]", 2007, 5, ItemType.FILM).substring(0, 17));
	}

	public void testTvSeries()
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, FailingHttpStatusCodeException, MalformedURLException, IOException {
		IMDbMock mock = new IMDbMock();
		String title = "Transformers 3";
		System.out.println(title + " URL: " + "http://www.imdb.com/find?ref_=nv_sr_fn&q="
				+ URLEncoder.encode(title, "UTF-8") + "&s=tt");
		//assertEquals(mock.returnUrl(title, 2011, 5, ItemType.FILM).substring(0, 17), "/title/tt1399103/");

		String title2 = "Dr House";
		//assertEquals(mock.returnUrl(title2, 2004, 5, ItemType.SERIES).substring(0, 17), "/title/tt0412142/");

	}

	public void testUrl() throws UnsupportedEncodingException {
		assertEquals("Red+2", URLEncoder.encode("Red 2", "UTF-8"));
	}

	public void testType() throws FailingHttpStatusCodeException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException {
		IMDbMock mock= new IMDbMock();
		String title = "Dr House";
		//assertEquals(mock.returnUrl(title, 2004, 5, ItemType.SERIES).substring(0, 17), "/title/tt0412142/");

		String title2 = "Crysis";
		//assertEquals(mock.returnUrl(title2, 2007, 5, ItemType.GAME).substring(0, 17), "/title/tt0954921/");

	}

	public void testIntensive()
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, FailingHttpStatusCodeException, MalformedURLException, IOException {
		IMDbMock mock= new IMDbMock();
		assertEquals("/title/tt1038988/", mock.returnUrl("[REc]", 2007, 5, ItemType.FILM).substring(0, 17));
	}

}
