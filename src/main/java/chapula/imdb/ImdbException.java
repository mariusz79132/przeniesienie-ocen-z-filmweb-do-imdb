package chapula.imdb;

public class ImdbException extends Exception {

	public ImdbException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
