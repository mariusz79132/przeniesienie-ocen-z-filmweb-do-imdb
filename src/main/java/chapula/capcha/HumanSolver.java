package chapula.capcha;

import java.io.ByteArrayInputStream;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class HumanSolver implements CaptchaSolver {

	@Override
	public String solveCaptcha(byte[] imageArray) {

		final StringBuilder result = new StringBuilder();
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Stage popupwindow = new Stage();
				popupwindow.initModality(Modality.APPLICATION_MODAL);
				popupwindow.setTitle("Type in captcha");
				
				final TextField captchaResult = new TextField();

				Image captcha = new Image(new ByteArrayInputStream(imageArray));
				ImageView iv2 = new ImageView(captcha);
				Button send = new Button("Send");

				send.setOnAction(e -> popupwindow.close());
				captchaResult.setOnKeyPressed(e -> {
					if (e.getCode() == KeyCode.ENTER){
						popupwindow.close();
					}
				});

				VBox layout = new VBox(10);
				layout.getChildren().addAll(iv2, captchaResult, send);
				layout.setAlignment(Pos.CENTER);
				Scene scene1 = new Scene(layout, captcha.getWidth(), captcha.getHeight() + 80);
				popupwindow.setScene(scene1);
				popupwindow.requestFocus();
				popupwindow.showAndWait();
				result.append(captchaResult.getText());
			}
		});

		while (result.length() == 0) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {

			}
		}

		return result.toString();
	}
}
