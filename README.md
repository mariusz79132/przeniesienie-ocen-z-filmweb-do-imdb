# README #

Projekt przenosi oceny z konta filmwebu na portal IMDb. 

### Użycie ###
Plik filmweb-x.x-jar-with-dependencies.jar znajduje się w folderze target gdzie x.x oznacza aktualną wersję. 

java -jar filmweb-x.x-jar-with-dependencies.jar [[-saveMovies filmwebUsername filmwebPassword] [-loadMovies imdbUsername imdbPassword] [-loadAndSave filmwebUsername filmwebPassword imdbUsername imdbPassword]]

-loadAndSave filmwebUsername filmwebPassword imdbUsername imdbPassword - zapis ocen filmów z filmweba -> IMDb

-saveMovies filmwebUsername filmwebPassword - zapis ocen filmów z Filmweba do pliku

-loadMovies imdbUsername imdbPassword - zapis ocen filmów z pliku do IMDb

Szczegółowy log znajduje się w pliku filmweb.log. 

Plik notFound.txt zawiera tytuły nie znalezione na IMDb.

### Wersja ###

1.0 - pierwsza wersja

1.1 - działanie captchy

1.2 - Odległość Levenshteina użyta do wyświetlania podobnych tytułów w przypadku nieznalezienia filmu. Poprawa odnajdywania tytułów i szybkości działania

1.3 - Poprawne dodawanie typu ( film/serial/gra ). Wyszukiwanie filmmu po tytule polskim / angielskim

1.3.1 - Poprawa wczytywania znaków z wiersza poleceń. Niektóre filmy oznaczone na filmweb jako filmy są oznaczone jako TV w IMDb dlatego przy nieznalezieniu filmu algorytm przeszukuje również TV. 

1.4 - kolejka na nieznalezione filmy

Poprawa działania captchy


### Wczytanie projektu ###
Jest to projekt typu "Maven project". Aby utworzyć plik jar należy użyć komendy mvn package.

Projekt wykorzystuje poniższe bibliteki do działania: 

- htmlunit

- FilmwebApi

Instrukcja instanacji biblioteki FilmwebApi znajduje się w poniższym odnośniku: https://bitbucket.org/varabi/filmweb-api

### Szczegóły odjandywania filmów ###

1. Program zapisuje szczegółowe informacje o filmie takie jak:
    1. Polski tytuł 
    2. Angielski tytuł
    3. Rok wydania
    4. Ocena użytkownika
2. Na stronie IMDb wyszukane zostają tytuły po angielsku oraz po polsku. 

    1. Ze strony wyszukiwania pobierane są tytuły filmów w dwóch formatach. Tytuły te następnie przechodzą przez preprocesig gdzie usuwane są znaki spejalne, duże litery zamieniane są na małe, polskie znaki zamieniane są na angielskie ( ł na l, ą na a ) ( "Łzy słońca" na filmweb -> "Lzy slonca" na imdb").

3. W przypadku nieznalezienia tytułu program przeszukuje stronę pod kątem ostatnich 2 lat w przód lub tył od roku wydania ( rozbieżność bazy filmweb -> imdb ). A następnie wyświetla do 4 filmów według odległości Levenshteina od obu tytułów( przykład zakłamania to "pyjamas" na filmweb -> "pajamas" na imdb).
    1. Użytkownik może wybrać właściwy tytuł wpisując numer widoczny w nawiasach kwadratowych. 
    2. Użytkonik może pominąć film wpisując p 
    3. Użytkownik ma również możliwość podania właściwego URL do pliku wpisując adres prawidłowego filmu ( Adres ten musi zaczynać się od http://imdb.com lub http://www.imdb.com ). Pominięte filmy zapisywane są do pliku notFound.txt




