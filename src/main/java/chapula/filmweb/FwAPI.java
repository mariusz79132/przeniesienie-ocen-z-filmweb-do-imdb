package chapula.filmweb;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import info.talacha.filmweb.api.FilmwebApi;
import info.talacha.filmweb.connection.FilmwebException;
import info.talacha.filmweb.models.Item;
import info.talacha.filmweb.models.User;
import info.talacha.filmweb.models.Vote;
import javafx.util.Pair;

public class FwAPI {
	FilmwebApi fa = new FilmwebApi();
	private String username = null;
	private String password = null;
	final static Logger logger = Logger.getLogger(FwAPI.class);

	public FwAPI(String filmwebUsername, String filmwebPassword) {
		this.username = filmwebUsername;
		this.password = filmwebPassword;
	}


	public List<Pair<Item, Integer>> download() throws FilmwebException {
		User user = fa.login(username, password);
		List<Vote> votes = fa.getUserVotes(user.getId(), 0, 20);

		List<Pair<Item, Integer>> votesList = new LinkedList<>();

		logger.info("Votes");
		for (Vote vote : votes) {

			Item f = null;
			switch (vote.getType().name()) {
			case "FILM": {
				f = fa.getFilmData(vote.getItemId());
				break;
			}
			case "GAME": {
				f = fa.getGameData(vote.getItemId());
				break;
			}
			case "SERIES": {
				f = fa.getSeriesData(vote.getItemId());
				break;
			}
			default: {
				logger.error("Wrong type");
			}

			}

			if (f.getTitle() != null) {
				votesList.add(new Pair<Item, Integer>(f, vote.getRate()));
				logger.info(String.format("%s (%s) Type: %s Rate: %s", f.getTitle(), f.getYear(),
						f.getClass().getSimpleName(), vote.getRate()));
			} else {
				votesList.add(new Pair<Item, Integer>(f, vote.getRate()));
				logger.info(String.format("%s (%s) Type: %s Rate: %s", f.getPolishTitle(), f.getYear(),
						f.getClass().getSimpleName(), vote.getRate()));

			}

		}

		return votesList;
	}

	public static void save(List<Pair<Item, Integer>> list) {
		FileOutputStream fout;
		ObjectOutputStream oos = null;
		try {
			fout = new FileOutputStream("predictions.obj");

			oos = new ObjectOutputStream(fout);
			oos.writeObject(list);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
