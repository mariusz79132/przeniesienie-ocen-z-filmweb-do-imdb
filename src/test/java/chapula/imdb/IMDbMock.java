package chapula.imdb;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;

import info.talacha.filmweb.models.Film;
import info.talacha.filmweb.models.Game;
import info.talacha.filmweb.models.Item;
import info.talacha.filmweb.models.ItemType;
import info.talacha.filmweb.models.Series;

public class IMDbMock {

	final WebClient wc = new WebClient();

	public IMDbMock() {
		wc.getOptions().setThrowExceptionOnFailingStatusCode(false);
		wc.getOptions().setJavaScriptEnabled(false);
		wc.addRequestHeader("Accept-Language", "pl, en-us");
	}

	public Item makeItem(String title, int year, ItemType type) {

		List<String> itemInfo = new LinkedList<>();

		for (int i = 0; i < 30; i++) {
			itemInfo.add(i, null);
		}
		itemInfo.set(1, title);
		itemInfo.set(5, "" + year);

		Item item = null;

		switch (type.name()) {
		case "FILM": {
			item = new Film(itemInfo);
			System.out.println("Test filmu " + title);
			break;
		}
		case "SERIES": {
			item = new Series(itemInfo);
			System.out.println("Test serialu " + title);
			break;
		}
		case "GAME": {
			item = new Game(itemInfo);
			System.out.println("Test gry " + title);
			break;
		}
		default: {
			System.out.println("Wrong type");
		}
		}
		return item;
	}

	public String returnUrl(String title, int year, int rate, ItemType type)
			throws FailingHttpStatusCodeException, IOException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		Imdb imdb = new Imdb("testU", "TestP");
		Class[] cArg = new Class[2];
		cArg[0] = Item.class;
		cArg[1] = int.class;

		Method method = Imdb.class.getDeclaredMethod("findItem", cArg);
		method.setAccessible(true);

		return (String) method.invoke(imdb, makeItem(title, year, type), rate);
	}

	public void savePage(String link) throws FailingHttpStatusCodeException, IOException {
		WebRequest myRequest = new WebRequest(new URL(link));
		myRequest.setAdditionalHeader("Accept-Language", "pl");
		String contentAsString = wc.getPage(myRequest).getWebResponse().getContentAsString();

		try (PrintWriter out = new PrintWriter("test.html")) {
			out.println(contentAsString);
		}
	}

	public NotFoundStack getStack(String response)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Imdb imdb = new Imdb("", "");
		Field notFoundField = imdb.getClass().getDeclaredField("notFoundStack");
		notFoundField.setAccessible(true);
		NotFoundStack notFoundStack = (NotFoundStack) notFoundField.get(imdb);

		Field inputStream = notFoundStack.getClass().getDeclaredField("inputStream");
		inputStream.setAccessible(true);
		inputStream.set(notFoundStack, getInputStream(response));

		return notFoundStack;
	}

	public InputStream getInputStream(String command) {
		InputStream in = new ByteArrayInputStream(command.getBytes());
		System.setIn(in);
		return in;
	}

	public String returnUrl(Item item) throws FailingHttpStatusCodeException, IOException, NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		Imdb imdb = new Imdb("testU", "TestP");
		Class[] cArg = new Class[1];
		cArg[0] = Item.class;

		Method method = Imdb.class.getDeclaredMethod("findItem", cArg);
		method.setAccessible(true);

		String returnS = (String) method.invoke(imdb, item);
		System.out.println(returnS);
		return returnS;
	}
}
