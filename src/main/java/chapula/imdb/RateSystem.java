package chapula.imdb;

import java.net.URL;

import org.apache.log4j.Logger;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class RateSystem {
	WebClient wc = null;

	public RateSystem(WebClient wc) {
		super();
		this.wc = wc;
	}

	final static Logger logger = Logger.getLogger(RateSystem.class);

	public void rateMovie(String url, int rate) throws Exception {

		logger.info("Rating url: " + url + " rate: " + rate);

		HtmlPage pageMovie = wc.getPage(url);
		DomElement ratingDiv = pageMovie.getElementById("star-rating-widget");
		WebRequest requestSettings = new WebRequest(new URL("http://www.imdb.com/ratings/_ajax/title"),
				HttpMethod.POST);

		String tconst = ratingDiv.getAttribute("data-tconst");
		String auth = ratingDiv.getAttribute("data-auth");

		if (auth.equals("") || auth == null) {
			logger.debug(ratingDiv);
			throw new Exception("No auth");
		}
		String body = String.format(
				"tconst=%s&rating=%d&auth=%s&tracking_tag=title-maindetails&pageId=%s&pageType=title&subpageType=main",
				tconst, rate, auth, tconst);
		requestSettings.setRequestBody(body);

		logger.debug("Body: " + body);
		Page redirectPage = wc.getPage(requestSettings);
		logger.debug("Rating response: " + redirectPage.getWebResponse().getContentAsString());
	}
}
