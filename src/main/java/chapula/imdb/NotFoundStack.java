package chapula.imdb;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.log4j.Logger;

import chapula.string.Levenstein;
import chapula.string.Parse;
import info.talacha.filmweb.models.Item;
import javafx.util.Pair;

public class NotFoundStack {
	final static Logger logger = Logger.getLogger(NotFoundStack.class);

	private Queue<ItemInfo> notFoundItem = new LinkedBlockingDeque<>();
	private BlockingDeque<Pair<String, Integer>> notFoundLinks = new LinkedBlockingDeque<>();
	private File notFoundFile = null;
	private RateSystem rateSystem = null;
	private InputStream inputStream = System.in;
	private boolean stoppedAdding = false;

	public NotFoundStack(RateSystem rateSystem, File notFoundFile) {
		this.notFoundFile = notFoundFile;
		this.rateSystem = rateSystem;
	}

	public void addMovie(Item item, Set<IMDbFilm> movies, int rate) {
		notFoundItem.add(new ItemInfo(item, movies, rate));
	}

	class RateRunnable implements Runnable {
		@Override
		public void run() {
			for (;;) {
				try {
					Pair<String, Integer> itemInfo = notFoundLinks.take();
					rateSystem.rateMovie(itemInfo.getKey(), itemInfo.getValue());
				} catch (InterruptedException e) {
					logger.error("Error: ", e);
				} catch (Exception e) {
					logger.error("Error: ", e);
				} finally {
					if (stoppedAdding && notFoundLinks.isEmpty()) {
						logger.info("Stopped adding");
						break;
					}
				}
			}

		}
	}

	public void rate() throws InterruptedException {
		logger.info("Rating not found movies, size: " + notFoundItem.size());
		Thread rateThread = new Thread(new RateRunnable());
		rateThread.start();

		try (Scanner sc = new Scanner(inputStream)) {
			notFoundItem.forEach(itemInfo -> {
				Item item = itemInfo.getItem();
				String partUrl = chooseOne(sc, item, itemInfo.getMovies());
				if (partUrl == null) {
					try {
						String title = item.getPolishTitle() == null ? item.getTitle() : item.getPolishTitle();
						Files.write(Paths.get(notFoundFile.getPath()),
								String.format("%s%n", (title + " (" + item.getYear() + ")")).getBytes(),
								StandardOpenOption.APPEND);

					} catch (IOException e2) {
						logger.error(e2.getMessage());
					}
				} else {
					notFoundLinks.add(new Pair<>("http://imdb.com" + partUrl, itemInfo.getRate()));
				}
			});
		}

		stoppedAdding = true;

		try {
			logger.info("Waiting for thread to finish");
			rateThread.join();
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
		}
	}

	private String chooseOne(Scanner sc, Item item, Set<IMDbFilm> movies) {
		List<Pair<IMDbFilm, Integer>> moviesLevenstein = new LinkedList<>();
		String title = "";

		if (item.getTitle() != null) {
			moviesLevenstein.addAll(Levenstein.makeLevensteinList(item.getTitle(), movies));
			title += item.getTitle();
		}

		if (item.getPolishTitle() != null) {
			moviesLevenstein.addAll(Levenstein.makeLevensteinList(item.getPolishTitle(), movies));
			title += " | " + item.getPolishTitle();
		}

		Levenstein.sort(moviesLevenstein);
		Levenstein.removeDuplicates(moviesLevenstein);

		if (moviesLevenstein.size() == 0)
			return null;

		int[] pos = { 0 };

		logger.info("--------------");
		logger.info("Movie: " + title);
		logger.info("Type number in bracket to save this item, p to skip or link to rate it:");
		moviesLevenstein.stream().limit(4).forEach(movie -> logger.info(String.format("[%d] %s (%d) http://imdb.com%s",
				pos[0]++, movie.getKey().getName(), movie.getKey().getYear(), movie.getKey().getUrl())));

		Integer position = null;
		try {
			if (sc.hasNext()) {
				String line = sc.next();
				logger.debug("Typed: " + line);
				if (line.startsWith("http://imdb.com")) {
					return line.substring(15, line.length());
				} else if (line.startsWith("http://www.imdb.com")) {
					return line.substring(19, line.length());
				} else if (Parse.tryParseInt(line)) {
					position = Integer.parseInt(line);

					// FIXME moze byc wieksze niz 4
					if (position < 0 || moviesLevenstein.size() - 1 < position)
						return null;
				} else {
					return null;
				}

			}

		} catch (InputMismatchException ex) {
			if (item.getTitle() != null) {
				logger.warn("Item skipped: " + item.getTitle());
			} else if (item.getPolishTitle() != null) {
				logger.warn("Item skipped: " + item.getPolishTitle());
			}

			return null;
		} finally {
			sc.nextLine();
		}
		return moviesLevenstein.get(position).getKey().getUrl();
	}
}
