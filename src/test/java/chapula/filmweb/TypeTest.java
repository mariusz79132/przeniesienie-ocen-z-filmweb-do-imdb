package chapula.filmweb;

import java.util.List;

import info.talacha.filmweb.api.FilmwebApi;
import info.talacha.filmweb.connection.FilmwebException;
import info.talacha.filmweb.models.ItemType;
import info.talacha.filmweb.search.models.FilmSearchResult;
import junit.framework.TestCase;

public class TypeTest extends TestCase {
	public void testMovie() throws FilmwebException {
		FilmwebApi fa = new FilmwebApi();

		List<FilmSearchResult> filmInfoList = fa.findFilm("aliens", 1986);

		for (FilmSearchResult film : filmInfoList) {
			System.out.println("Type: " + film.getClass().getName());
			assertEquals(film.getType().name(), ItemType.FILM.name());

		}
	}
}
