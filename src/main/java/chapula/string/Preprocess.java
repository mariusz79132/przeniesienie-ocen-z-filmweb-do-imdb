package chapula.string;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class Preprocess {

	public static String deAccent(String str) {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("").replace("ł", "l");
	}

	public static String preprocessTitle(String title) {
		return title == null ? ""
				: deAccent(title.replaceAll(
						"[\\[\\@\\]\\\\\\[\\\"\\#\\!\\&\\'\\`\\%\\*\\+\\(\\)\\/\\-\\;\\~\\:\\}\\?\\>\\=\\<\\]\\.\\,]",
						"").toLowerCase());
	}
}
