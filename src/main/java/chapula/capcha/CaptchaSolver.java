package chapula.capcha;

public interface CaptchaSolver {

	public String solveCaptcha(byte[] imageBytes);
}
