package chapula.string;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import chapula.imdb.IMDbFilm;
import javafx.util.Pair;

public class Levenstein {

	public static List<Pair<IMDbFilm, Integer>> makeLevensteinList(String preprocessTitle, Set<IMDbFilm> movies) {
		List<Pair<IMDbFilm, Integer>> moviesLevenstein = new LinkedList<>();
		for (IMDbFilm movie : movies) {
			moviesLevenstein.add(
					new Pair<IMDbFilm, Integer>(movie, Levenstein.count(movie.getPreprocessedName(), preprocessTitle)));
		}
		return moviesLevenstein;
	}

	public static void sort(List<Pair<IMDbFilm, Integer>> list) {
		Collections.sort(list, new Comparator<Pair<IMDbFilm, Integer>>() {
			@Override
			public int compare(Pair<IMDbFilm, Integer> o1, Pair<IMDbFilm, Integer> o2) {
				Integer a = o2.getValue();
				Integer b = o1.getValue();

				if (a.compareTo(b) > 0)
					return -1;
				else if (a.compareTo(b) < 0)
					return 1;
				else
					return 0;
			}
		});

	}

	public static void removeDuplicates(List<Pair<IMDbFilm, Integer>> list) {
		int size = list.size();

		for (int i = 0; i < size - 1; i++) {

			for (int j = i + 1; j < size; j++) {

				if (!list.get(j).getKey().getUrl().equals(list.get(i).getKey().getUrl()))
					continue;

				list.remove(j);
				j--;
				size--;
			}
		}
	}

	public static int count(String first, String second) {

		int sizeX = first.length() + 1;
		int sizeY = second.length() + 1;

		int[][] array = new int[sizeX][sizeY];

		for (int i = 0; i < sizeX; i++)
			array[i][0] = i;

		for (int i = 1; i < sizeY; i++)
			array[0][i] = i;

		for (int i = 1; i < sizeX; i++)
			for (int j = 1; j < sizeY; j++) {
				array[i][j] = min(array[i][j - 1], array[i - 1][j], array[i - 1][j - 1]);
				// System.out.println(first.length()+" "+second.length()+" "+i+"
				// "+j);

				if (first.charAt(i - 1) != second.charAt(j - 1)) {
					array[i][j]++;
				}

			}
		return array[sizeX - 1][sizeY - 1];
	}

	private static int min(int a, int b, int c) {
		int min = a;
		if (min > b) {
			min = b;
		}
		if (min > c) {
			min = c;
		}

		return min;
	}

}
