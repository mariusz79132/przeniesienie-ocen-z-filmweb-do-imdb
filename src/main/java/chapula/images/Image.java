package chapula.images;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Image {

	public static String fileMD5(byte[] imageBytes) throws NoSuchAlgorithmException, IOException {

		MessageDigest md5Digest = MessageDigest.getInstance("MD5");

		String checksum = getFileChecksum(md5Digest, imageBytes);

		return checksum;
	}

	private static String getFileChecksum(MessageDigest digest, byte[] imageBytes) throws IOException {

		digest.update(imageBytes, 0, imageBytes.length);

		// Get the hash's bytes
		byte[] bytes = digest.digest();

		// This bytes[] has bytes in decimal format;
		// Convert it to hexadecimal format
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		// return complete hash
		return sb.toString();
	}
}
