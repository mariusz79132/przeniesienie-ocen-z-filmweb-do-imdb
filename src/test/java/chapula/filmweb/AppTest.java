package chapula.filmweb;

import java.io.UnsupportedEncodingException;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	public void testUrlDecode() throws UnsupportedEncodingException {
		assertEquals(java.net.URLDecoder.decode("%3D", "UTF-8"), "=");
	}

	public void testPattern() {
		System.out.println("(?s)<span class=\"a-list-item\">\\s+(.*?)\\s+<\\/span>");
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testApp() {
		assertTrue(true);
	}
}
