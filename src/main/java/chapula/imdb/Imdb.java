package chapula.imdb;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CookieManager;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.HtmlEmailInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

import chapula.capcha.CaptchaSolver;
import chapula.capcha.HumanSolver;
import chapula.string.Preprocess;
import info.talacha.filmweb.models.Item;
import javafx.util.Pair;

public class Imdb implements Closeable {

	private final static Logger logger = Logger.getLogger(Imdb.class);
	private File notFoundFile = new File("notFound.txt");
	private RateSystem rateSystem = null;
	private NotFoundStack notFoundStack = null;
	private List<Pair<Item, Integer>> pred = new LinkedList<>();
	private WebClient wc = null;
	private String username = null;
	private String password = null;

	public List<Pair<Item, Integer>> getRates() {
		return pred;
	}

	private Imdb() {
		newWC();

		rateSystem = new RateSystem(wc);
		notFoundStack = new NotFoundStack(rateSystem, notFoundFile);
		try {
			if (!notFoundFile.exists()) {
				notFoundFile.createNewFile();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public Imdb(String imbdUsername, String imbdPassword) {
		this();
		this.username = imbdUsername;
		this.password = imbdPassword;
	}

	public void signIn() throws Exception {
		wc.getOptions().setJavaScriptEnabled(true);

		logger.info("Signing in");
		HtmlPage page = wc.getPage(
				"https://www.imdb.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.imdb.com%2Fap-signin-handler&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=imdb_us&openid.mode=checkid_setup&siteState=eyJyZWRpcmVjdFRvIjoiaHR0cHM6Ly93d3cuaW1kYi5jb20vP3JlZl89bG9naW4ifQ&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&&tag=imdbtag_reg-20");
		final HtmlEmailInput usernameF = (HtmlEmailInput) page.getElementById("ap_email");
		final HtmlPasswordInput passwordF = (HtmlPasswordInput) page.getElementById("ap_password");

		usernameF.setValueAttribute(username);
		page.getElementById("ap_email").setNodeValue(password);
		passwordF.setValueAttribute(password);
		page.getElementById("ap_password").setNodeValue(password);
		HtmlPage pageLoaded = page.getElementById("signInSubmit").click();

		// logger.debug(pageLoaded.getWebResponse().getContentAsString());
		// captcha
		String gifUrl = findUrl(pageLoaded.getWebResponse().getContentAsString());
		while (gifUrl != null) {
			CaptchaSolver solver = new HumanSolver();
			final HtmlTextInput captcha2 = (HtmlTextInput) pageLoaded.getElementById("auth-captcha-guess");
			captcha2.focus();
			InputStream image = wc.getPage(gifUrl).getWebResponse().getContentAsStream();
			byte[] imageBytes = IOUtils.toByteArray(image);
			logger.debug("Image byte array length: " + imageBytes.length);
			logger.info("Type in captcha");
			String captcha = solver.solveCaptcha(imageBytes);
			logger.debug("Returned captcha: " + captcha);
			if (captcha != null) {
				setUsernamePassword(pageLoaded);
				captcha2.setValueAttribute(captcha);
				pageLoaded = pageLoaded.getElementById("signInSubmit").click();
			}
			gifUrl = findUrl(pageLoaded.getWebResponse().getContentAsString());

			System.out.println(getResponseMessage(pageLoaded.getWebResponse().getContentAsString()));
		}

		if (getResponseMessage(pageLoaded.getWebResponse().getContentAsString()) != null) {
			logger.error("Not logged in: ");
			throw new Exception(getResponseMessage(pageLoaded.getWebResponse().getContentAsString()));
		} else {
			logger.info("Succesfully logged in");
			wc.getOptions().setJavaScriptEnabled(false);
		}
		//
	}

	private void setUsernamePassword(HtmlPage page) {
		final HtmlPasswordInput passwordF = (HtmlPasswordInput) page.getElementById("ap_password");
		passwordF.focus();
		try {
			Thread.sleep(2_000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		passwordF.setValueAttribute(password);
		page.getElementById("ap_password").setNodeValue(password);

		// logger.debug(page.getWebResponse().getContentAsString());
	}

	private String getResponseMessage(String source) {

		Pattern pattern = Pattern.compile("(?s)<span class=\"a-list-item\">\\s+(.*?)\\s+<\\/span>");
		Matcher matcher = pattern.matcher(source);
		if (matcher.find()) {
			return matcher.group(1);
		} else {
			return null;
		}
	}

	private void newWC() {
		wc = new WebClient(BrowserVersion.FIREFOX_45);
		wc.getOptions().setThrowExceptionOnFailingStatusCode(false);
		wc.getOptions().setThrowExceptionOnScriptError(false);
		// wc.getOptions().setCssEnabled(false);
		wc.getOptions().setJavaScriptEnabled(false);
		wc.getOptions().setRedirectEnabled(true);
	}

	public void loadCookie() throws IOException, ClassNotFoundException {
		if (Files.exists(Paths.get("IMBDcookies"))) {
			logger.debug("Loading cookies");
			FileInputStream fileIn = new FileInputStream("IMBDcookies");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			wc.setCookieManager((CookieManager) in.readObject());
			in.close();
			fileIn.close();
		}
	}

	public void saveCookie() throws IOException {
		logger.debug("Saving cookies");
		FileOutputStream fileOut = new FileOutputStream("IMBDcookies");
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(fileOut);
			out.writeObject(wc.getCookieManager());
		} catch (Exception e) {
			logger.error(e);
		} finally {
			if (out != null)
				out.close();

			if (fileOut != null)
				fileOut.close();
		}
	}

	private String findUrl(String contentAsString) throws Exception {
		logger.info("Finding captcha url");
		final Pattern pattern = Pattern.compile("(?s)auth-captcha-image-container.*?(https:\\/\\/opfcaptcha-.*?)\"");
		final Matcher matcher = pattern.matcher(contentAsString);
		if (matcher.find()) {
			String url = matcher.group(1).replace("&amp;", "&");
			logger.debug("URL: " + url);
			return url;
		} else {
			logger.debug("Gif url not found");
			return null;
		}

	}

	private void appendItem(Item item, StringBuilder source, String className)
			throws FailingHttpStatusCodeException, IOException {
		if (item.getPolishTitle() != null) {
			source.append(getItemSource(item.getPolishTitle(), item.getClass().getSimpleName()));
		}

		if (item.getTitle() != null) {
			source.append(getItemSource(item.getTitle(), item.getClass().getSimpleName()));
		}
	}

	private void appendItem(Item item, StringBuilder source) throws FailingHttpStatusCodeException, IOException {
		appendItem(item, source, item.getClass().getSimpleName());
	}

	private String findItem(Item item, int rate) throws Exception {

		StringBuilder source = new StringBuilder();
		appendItem(item, source);

		final Set<IMDbFilm> movies = new HashSet<>();
		addAll(movies, item.getYear(), source.toString());

		for (IMDbFilm movie : movies) {
			logger.debug("Search name: " + movie.getPreprocessedName() + " Movie on site: "
					+ Preprocess.preprocessTitle(item.getTitle()) + " | "
					+ Preprocess.preprocessTitle(item.getPolishTitle()));
			String preprocessedName = movie.getPreprocessedName();
			if (preprocessedName.equals(Preprocess.preprocessTitle(Preprocess.preprocessTitle(item.getTitle())))
					|| preprocessedName
							.equals(Preprocess.preprocessTitle(Preprocess.preprocessTitle(item.getPolishTitle())))) {
				logger.debug("Search name: " + movie.getName() + " URL: " + movie.getUrl());
				return movie.getUrl();
			}

		}

		if (item.getClass().getSimpleName().equals("Film")) {
			appendItem(item, source, "Series");
		}

		addCommonDates(movies, item.getYear(), source.toString());
		notFoundStack.addMovie(item, movies, rate);
		return null;
	}

	private String getItemSource(String title, String type) throws FailingHttpStatusCodeException, IOException {

		URL url = null;
		logger.debug("Type: " + type);
		switch (type) {
		case "Game": {
			url = new URL(
					"http://www.imdb.com/find?q=" + URLEncoder.encode(title, "UTF-8") + "&s=tt&ttype=vg&ref_=fn_vg");
			break;
		}
		case "Film": {
			url = new URL(
					"http://www.imdb.com/find?q=" + URLEncoder.encode(title, "UTF-8") + "&s=tt&ttype=ft&ref_=fn_ft");
			break;
		}
		case "Series": {
			url = new URL(
					"http://www.imdb.com/find?q=" + URLEncoder.encode(title, "UTF-8") + "&s=tt&ttype=tv&ref_=fn_tv");
			break;
		}
		}

		logger.debug("Search url: " + url);

		WebRequest pageRequest = new WebRequest(url);
		pageRequest.setAdditionalHeader("Accept-Language", "pl");

		HtmlPage pageLoaded = wc.getPage(pageRequest);
		return pageLoaded.getWebResponse().getContentAsString();

	}

	private void addAll(Set<IMDbFilm> movies, int year, String source) {
		logger.debug("Getting results");
		final String patternS = "\"result_text\">[^>]*?href=\"([^>]*?)\"[^>]*?>([^<]+?)</a>[^<]*?\\(" + year + "\\)";
		final String patternS2 = "\"result_text\">[^>]*?href=\"([^>]*?)\"[^>]*?>([^>]*?)<\\/a>[^<]*?\\(" + year
				+ "\\) <br\\/>aka <i>\"([^<]+?)\"";

		logger.debug("Pattern1: " + patternS);
		logger.debug("Pattern2: " + patternS2);

		final Pattern pattern = Pattern.compile(patternS);
		final Pattern pattern2 = Pattern.compile(patternS2);
		final Matcher matcher = pattern.matcher(source);
		final Matcher matcher2 = pattern2.matcher(source);

		addAll(matcher, movies, year);
		addAll(matcher2, movies, year);
	}

	private void addCommonDates(Set<IMDbFilm> movies, int year, String source) {

		for (int newYear = year - 2; newYear <= year + 2; newYear++) {
			if (newYear != year) {
				addAll(movies, newYear, source);
			}
		}
	}

	private void addAll(Matcher matcher, Set<IMDbFilm> movies, int year) {
		while (matcher.find()) {
			logger.debug("Item name: " + matcher.group(2) + "  URL: " + matcher.group(1));
			movies.add(new IMDbFilm(matcher.group(2), matcher.group(1), year));
		}
	}

	@SuppressWarnings("unchecked")
	public void loadRates() {
		logger.info("Loading rates");
		FileInputStream fin;
		try {
			fin = new FileInputStream("predictions.obj");

			ObjectInputStream ois = new ObjectInputStream(fin);
			pred = (List<Pair<Item, Integer>>) ois.readObject();
			ois.close();

			logger.info("Items count: " + pred.size());

		} catch (IOException | ClassNotFoundException e) {
			logger.error(e);
		}
	}

	private void ratePair(Pair<Item, Integer> itemInfo) {

		Item item = itemInfo.getKey();
		String title = item.getPolishTitle() == null ? item.getTitle() : item.getPolishTitle();

		logger.info(String.format("Searching for %s: %s in: %d rate: %s", item.getClass().getSimpleName(), title,
				item.getYear(), itemInfo.getValue()));
		try {
			String urlPart = findItem(item, itemInfo.getValue());
			if (urlPart == null) {
				throw new ImdbException("Item not found");
			} else {
				String url = "http://imdb.com" + urlPart;
				rateSystem.rateMovie(url, itemInfo.getValue());
			}
		} catch (Exception e) {
			logger.error("Item not found: " + title + " in: " + item.getYear() + " rate: " + itemInfo.getValue());
		}
	}

	public void rate() throws FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException {

		try {
			saveCookie();
		} catch (IOException e) {
			e.printStackTrace();
		}

		pred.forEach(f -> {
			try {
				ratePair(f);
			} catch (FailingHttpStatusCodeException e) {
				logger.error(e);
			}

			// WebClient ma wycieki pamieci, dlatego lepiej czyscic za kazdym
			// razem
			final List<WebWindow> windows = wc.getWebWindows();

			for (final WebWindow wd : windows) {
				wd.getJobManager().removeAllJobs();
			}
			wc.close();
			System.gc();

			newWC();
		});

		notFoundStack.rate();
	}

	@Override
	public void close() throws IOException {
		logger.info("IMDb trying to close");
		wc.close();
		logger.info("IMDb Closed");
	}
}
